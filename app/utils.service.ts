import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';

@Injectable()
export class UtilsService {

  constructor() { }

  apiHeader() {
    const headers = new Headers();
    const authToken = localStorage.getItem('authenticationToken');
    headers.append('token', authToken);
    const options = new RequestOptions({ headers: headers});
    return options;
  }
}
