import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [AppService]
})
export class SignupComponent implements OnInit {

  constructor(
    public _appService: AppService,
    public _router: Router,

  ) { }

  public form: FormGroup;
  public firstName: any;
  public lastName: any;
  public surName: any;
  public email: any;
  public password: any;
  public contactNo: any;
  public address: any;
  public profilePicture: any;
  public birthDate: any;
  public errorMsg: any;
  public showError = false;
  public maxDate: any;
  ngOnInit() {
    this.maxDate = new Date().toISOString().split('T')[0];

  }

  signUpUser(form) {
    this.showError = false;

    if (form.valid) {
      const token = Math.random().toString(36).substring(7);
      const bodyJson = {};
      Object(bodyJson).firstname = this.firstName;
      Object(bodyJson).lastname = this.lastName;
      Object(bodyJson).email = this.email;
      Object(bodyJson).password = this.password;
      Object(bodyJson).contact = this.contactNo;
      Object(bodyJson).address = this.address;
      Object(bodyJson).birthdate = this.birthDate;
      Object(bodyJson).token = token;
      this._appService.checkAndSignUp(bodyJson).subscribe(async (response) => {
        if (Object(response) && Number(Object(response).status_code) === 200) {
          this._router.navigate(['/login']);
        } else if (Object(response) && Number(Object(response).status_code) === 202) {
          this.showError = true;
          this.errorMsg = 'Email-id already registered';
        }
      }, (error) => {
        throw error;
      });
    }
  }

  redirectLogin() {
    this._router.navigate(['/login']);
  }

}
