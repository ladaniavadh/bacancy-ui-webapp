import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AppService]
})
export class LoginComponent implements OnInit {

  constructor(
    public _appService: AppService,
    public _router: Router,

  ) { }

  public form: FormGroup;
  public email: any;
  public password: any;
  public errorMsg: any;
  public showError = false;
  ngOnInit() {
  }

  loginUser(form) {
    this.showError = false;

    if (form.valid) {
      const bodyJson = {};
      Object(bodyJson).email = this.email;
      Object(bodyJson).password = this.password;
      this._appService.checkAndLogin(bodyJson).subscribe(async (response) => {
        if (Object(response) && Number(Object(response).status_code) === 200) {
          localStorage.setItem('user_id', response.data[0].id);
          localStorage.setItem('authenticationToken', response.data[0].token);
          localStorage.setItem('is_loggedin', 'true');
          const a = localStorage.getItem('is_loggedin') === 'true';
          this._router.navigate(['/home']);
        } else if (Object(response) && Number(Object(response).status_code) === 202) {
          this.showError = true;
          this.errorMsg = 'Invalid Login Details.';
        } else {
          this.showError = true;
          this.errorMsg = 'Email-id not found';
        }
      }, (error) => {
        throw error;
      });
    }
  }

  redirectSignUp() {
    this._router.navigate(['/signup']);
  }
}
