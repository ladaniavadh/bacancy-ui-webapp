import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { UtilsService } from './utils.service';
import 'rxjs/Rx';
import { from } from 'rxjs/observable/from';

@Injectable()
export class AppService {

  constructor(
    public _http: Http,
    public _utilsService: UtilsService,
  ) { }

  checkAndLogin(body) {
    const url = 'http://localhost:3000/api/v1/users/login';
    return this._http.post(url, body)
      .map(response => {
        return response.json();
      });
  }

  checkAndSignUp(body) {
    const url = 'http://localhost:3000/api/v1/users/signup';
    return this._http.post(url, body)
      .map(response => {
        return response.json();
      });
  }

  fetchUserData(id) {
    let url = 'http://localhost:3000/api/v1/users/:id';
    url = url.replace(':id', id);
    return this._http.get(url, this._utilsService.apiHeader())
      .map(response => {
        return response.json();
      });
  }

  

}
