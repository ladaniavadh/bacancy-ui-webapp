import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [AppService]

})
export class HomeComponent implements OnInit, OnDestroy {

  constructor(
    public _appService: AppService
  ) { }

  public response: any;
  ngOnInit() {
    const userId = localStorage.getItem('user_id');
    this.fetchUserData(userId);
  }

  fetchUserData(userId) {
      this._appService.fetchUserData(userId).subscribe(async (response) => {
        if (Object(response) && Number(Object(response).status_code) === 200) {
          this.response = response.data[0];
        }
      }, (error) => {
        throw error;
      });
  }

  ngOnDestroy() {
    localStorage.clear();
  }

}
